package com.huxiu.yd;

import com.android.volley.Request;
import com.android.volley.Response;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.huxiu.yd.net.GsonRequest;
import com.huxiu.yd.net.NetworkConstants;
import com.huxiu.yd.net.model.User;
import com.huxiu.yd.net.responses.UserListResponse;
import com.huxiu.yd.view.AvatarView;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/7/30.
 */
public class EnrolledUserListActivity extends BaseActivity implements PullToRefreshBase
        .OnRefreshListener2<GridView> {
    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.gridview)
    PullToRefreshGridView list;

    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrolled_user_list);
        ButterKnife.inject(this);
        id = getIntent().getIntExtra("topic_id", 0);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(R.string.enrolled_users);
        adapter = new EnrolledUserAdapter();
        list.setAdapter(adapter);
        list.setOnRefreshListener(this);
        list.setState(PullToRefreshBase.State.INIT_REFRESHING, true);
        getItems(0);
    }

    private int currentPage = 0;

    private List<User> items = new ArrayList<>();

    private void getItems(final int page) {
        String url = NetworkConstants.TOPIC_URL;
        Map<String, String> params = new HashMap<>();
        params.put("act", "enroll");
        params.put("id", Integer.toString(id));
        params.put("page", Integer.toString(page));
        GsonRequest<UserListResponse> request = new GsonRequest<>(url, Request.Method.POST,
                UserListResponse.class, true, params, new Response
                .Listener<UserListResponse>() {
            @Override
            public void onResponse(UserListResponse response) {
                if (response.isSuccess()) {
                    if (page == 0) {
                        items.clear();
                    }
                    Collections.addAll(items, response.data);
                    adapter.notifyDataSetChanged();
                    currentPage = response.page;
                    refreshComplete();
                }
            }
        }, mErrorListener);
        mQueue.add(request);
    }

    private void refreshComplete() {
        list.postDelayed(new Runnable() {
            @Override
            public void run() {
                list.onRefreshComplete();
            }
        }, 100);
    }

    private EnrolledUserAdapter adapter;

    @Override
    public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
        getItems(0);
    }

    @Override
    public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
        getItems(currentPage + 1);
    }

    public class EnrolledUserAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            ViewHolder vh;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.enrolled_user_view, parent, false);
                vh = new ViewHolder(view);
                view.setTag(vh);
            }
            vh = (ViewHolder) view.getTag();
            User user = items.get(position);
            vh.avatar.setUserInfo(user.user_id, user.avatar, user.name);
            vh.name.setText(user.name);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ViewHolder vh = (ViewHolder) v.getTag();
                    vh.avatar.performClick();
                }
            });
            return view;
        }

        /**
         * This class contains all butterknife-injected Views & Layouts from layout file
         * 'user_list_item.xml'
         * for easy to all layout elements.
         *
         * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers
         * (http://github.com/avast)
         */
        class ViewHolder {
            @InjectView(R.id.avatar)
            AvatarView avatar;

            @InjectView(R.id.name)
            TextView name;

            ViewHolder(View view) {
                ButterKnife.inject(this, view);
            }
        }
    }
}
