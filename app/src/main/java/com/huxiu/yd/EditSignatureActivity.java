package com.huxiu.yd;

import com.android.volley.Request.Method;
import com.android.volley.Response.Listener;
import com.huxiu.yd.net.GsonRequest;
import com.huxiu.yd.net.NetworkConstants;
import com.huxiu.yd.net.responses.BaseResponse;
import com.huxiu.yd.utils.Settings;
import com.huxiu.yd.utils.Utils;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/19:下午10:56.
 */
public class EditSignatureActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.left_text)
    TextView leftText;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.right_image)
    ImageView rightImage;

    @InjectView(R.id.right_text)
    TextView rightText;

    @InjectView(R.id.edit)
    EditText edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_signature);
        ButterKnife.inject(this);
        back.setVisibility(View.GONE);
        leftText.setVisibility(View.VISIBLE);
        leftText.setText(R.string.cancel);
        leftText.setOnClickListener(this);
        title.setText(R.string.edit_profile);
        rightText.setVisibility(View.VISIBLE);
        rightText.setText(R.string.save);
        rightText.setOnClickListener(this);
        String signature = Settings.getSignature();
        if (!TextUtils.isEmpty(signature)) {
            edit.setText(signature);
        }
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                rightText.setEnabled(s != null && s.length() > 0);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.left_text:
                finish();
                break;
            case R.id.right_text:
                submitSignature();
                break;
        }
    }

    private void submitSignature() {
        String url = NetworkConstants.MEMBER_URL;
        Map<String, String> params = new HashMap<>();
        params.put("act", "up_user_info");
        params.put("yijuhua", edit.getText().toString());
        GsonRequest<BaseResponse> request = new GsonRequest<>(url, Method.POST, BaseResponse.class,
                true, params, new Listener<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                if (response.isSuccess()) {
                    Utils.showToast(R.string.submit_signature_success);
                    Settings.saveSignature(edit.getText().toString());
                    finish();
                } else {
                    Utils.showToast(response.msg);
                }
            }
        }, mErrorListener);
        showProgress();
        mQueue.add(request);
//        mQueue.start();
    }
}
