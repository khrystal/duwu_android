package com.huxiu.yd;

import com.android.volley.Request.Method;
import com.android.volley.Response.Listener;
import com.huxiu.yd.net.GsonRequest;
import com.huxiu.yd.net.NetworkConstants;
import com.huxiu.yd.net.responses.BaseResponse;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Utils;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/20:上午9:21.
 */
public class SubmitCommentActivity extends BaseActivity implements OnClickListener {
    @InjectView(R.id.content)
    protected View content;

    @InjectView(R.id.background)
    protected View background;

    @InjectView(R.id.submit_button)
    TextView submitButton;

    @InjectView(R.id.edit)
    EditText edit;

    private String articleId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        setContentView(R.layout.activity_submit_comment);
        articleId = getIntent().getStringExtra(Constants.ARTICLE_ID_KEY);
        ButterKnife.inject(this);
        background.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_CANCEL
                        || event.getAction() == MotionEvent.ACTION_UP) {
                    if (TextUtils.isEmpty(edit.getText().toString())) {
                        background.requestFocus();
                        finish();
                    } else {
                        new Builder(SubmitCommentActivity.this).setTitle(R.string.content_not_empty)
                                .setPositiveButton(android.R.string.ok,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                background.requestFocus();
                                                finish();
                                            }
                                        }).setNegativeButton(android.R.string.cancel, null).show();
                    }
                }
                return true;
            }
        });
        submitButton.setOnClickListener(this);
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                submitButton.setEnabled(s != null && s.length() > 0);
            }
        });
        InputMethodManager imm = (InputMethodManager) edit.getContext().getSystemService(Context
                .INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_button:
                submitComment();
                break;
        }
    }

    private void submitComment() {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("act", "add_comment");
        params.put("article_id", articleId);
        params.put("comment", edit.getText().toString());
        GsonRequest<BaseResponse> request = new GsonRequest<>(NetworkConstants.DISCOVERY_URL,
                Method.POST, BaseResponse.class, true, params, new Listener<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                dismissProgress();
                if (response.isSuccess()) {
                    Utils.showToast(R.string.submit_comment_success);
                    setResult(RESULT_OK);
                    finish();
                } else {
                    Utils.showToast(response.msg);
                }
            }
        }, mErrorListener);
        mQueue.add(request);
//        mQueue.start();
    }
}
