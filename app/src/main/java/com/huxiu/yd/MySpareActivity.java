package com.huxiu.yd;

import com.huxiu.yd.fragments.SpareFragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/18:下午10:30.
 */
public class MySpareActivity extends Activity {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.fragments)
    FrameLayout fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_spare);
        ButterKnife.inject(this);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(R.string.my_spare);
        Bundle bundle = new Bundle();
        bundle.putBoolean("topic", true);
        bundle.putBoolean("mine", true);
        Fragment fragment = Fragment.instantiate(this, SpareFragment.class.getName(), bundle);
        getFragmentManager().beginTransaction().add(R.id.fragments, fragment).show(fragment)
                .commitAllowingStateLoss();
    }
}
