package com.huxiu.yd;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.huxiu.yd.net.GsonRequest;
import com.huxiu.yd.net.NetworkConstants;
import com.huxiu.yd.net.NetworkHelper;
import com.huxiu.yd.net.responses.BindPushResponse;
import com.huxiu.yd.utils.LogUtils;
import com.huxiu.yd.utils.Settings;
import com.xiaomi.mipush.sdk.ErrorCode;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MiPushReceiver extends PushMessageReceiver {
    @Override
    public void onReceiveMessage(Context context, MiPushMessage miPushMessage) {
        LogUtils.d("onReceiveMessage, message is " + miPushMessage);
    }

    @Override
    public void onCommandResult(Context context, MiPushCommandMessage message) {
    }

    @Override
    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        super.onReceivePassThroughMessage(context, miPushMessage);
    }

    @Override
    public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageClicked(context, miPushMessage);
    }

    @Override
    public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        super.onNotificationMessageArrived(context, miPushMessage);
    }

    @Override
    public void onReceiveRegisterResult(Context context, MiPushCommandMessage
            message) {
        super.onReceiveRegisterResult(context, message);
        String command = message.getCommand();
        List<String> arguments = message.getCommandArguments();
        LogUtils.d("onReceiveRegisterResult, arguments: " + arguments);
        String miId = ((arguments != null && arguments.size() > 0) ? arguments.get(0) : null);
        if (MiPushClient.COMMAND_REGISTER.equals(command)) {
            if (message.getResultCode() == ErrorCode.SUCCESS) {
                Settings.setMiId(miId);
                registerPush(miId, "open");
            }
        }
    }

    public static void registerPush(String id, String type) {
        String url = NetworkConstants.MEMBER_URL;
        Map<String, String> params = new LinkedHashMap<>();
        params.put("xmuserid", id);
        params.put("act", "add_push_data");
        params.put("type", type);
        GsonRequest<BindPushResponse> request = new GsonRequest<>(url, Request.Method.POST,
                BindPushResponse.class, true, params, new Response.Listener<BindPushResponse>() {
            @Override
            public void onResponse(BindPushResponse response) {
                if (response.recommend_friend != null) {
                    Settings.setRecommend(response.recommend_friend);
                }
            }
        }, null);
        NetworkHelper.getInstance().addToRequestQueue(request);
    }

}
