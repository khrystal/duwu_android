package com.huxiu.yd;

import com.huxiu.yd.net.model.Province;
import com.huxiu.yd.net.model.Province.City;
import com.huxiu.yd.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by tian on 15/5/23:下午11:46.
 */
public class SelectAreaActivity extends Activity
        implements View.OnClickListener, AdapterView.OnItemClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.list)
    ListView list;

    private int type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_area);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        list.setOnItemClickListener(this);
        adapter = new ProvinceAdapter();
        list.setAdapter(adapter);
        Collections.addAll(provinces, Utils.getProvinces());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }

    private List<Province> provinces = new ArrayList<>();

    private Province selectedProvince;

    private class ProvinceAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return type == 0 ? provinces.size() : selectedProvince.city.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.city_selector_list_item, parent, false);
            }
            TextView tv = (TextView) view;
            Province province = provinces.get(position);
            if (type == 0) {
                tv.setText(province.province);
            } else {
                tv.setText(selectedProvince.city[position].city);
            }
            tv.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                    (province.city != null && province.city.length > 1 && type == 0)
                            ? R.drawable.right_arrow : 0, 0);
            return view;
        }
    }

    private ProvinceAdapter adapter;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (type == 0) {
            selectedProvince = provinces.get(position);
            type = 1;
            adapter.notifyDataSetChanged();
        } else {
            City city = selectedProvince.city[position];
            Intent intent = new Intent();
            intent.putExtra("province_name", selectedProvince.province);
            intent.putExtra("province_id", selectedProvince.id);
            intent.putExtra("city_name", city.city);
            intent.putExtra("city_id", city.id);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
