package com.huxiu.yd;

import com.huxiu.yd.net.NetworkHelper;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Global;
import com.huxiu.yd.utils.Settings;
import com.huxiu.yd.utils.Utils;
import com.nostra13.universalimageloader.cache.memory.impl.LRULimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.umeng.analytics.MobclickAgent;
import com.xiaomi.mipush.sdk.MiPushClient;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Application;
import android.content.Context;
import android.graphics.Point;
import android.os.Process;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.view.Display;
import android.view.WindowManager;

import java.util.List;

/**
 * Created by kHRYSTAL on 15/7/23:下午5:10.
 */
public class App extends Application {

    private static final int IMAGE_LOADER_THREAD_POOL_SIZE = 3;

    private static final int MEMORY_CACHE_SIZE = 2 * 1024 * 1024;

    private static final int DISC_CACHE_SIZE = 100 * 1024 * 1024;

    private static App sInstance;

    public static App getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        if (shouldInit()) {
            NetworkHelper.init();
            WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point point = new Point();
            display.getSize(point);
            Global.screenWidth = point.x;
            Global.preferredImageHeight = point.x * 9 / 16;
            Global.preferredImageHeight2 = point.x * 7 / 16;
            Global.DeviceUniqueID = Settings.getUniqueDeviceId();
            if (Global.DeviceUniqueID == null) {
                Global.DeviceUniqueID = android.provider.Settings.Secure
                        .getString(getContentResolver(),
                                android.provider.Settings.Secure.ANDROID_ID);
                if (Global.DeviceUniqueID == null) {
                    Global.DeviceUniqueID = String.valueOf(System.currentTimeMillis());
                }
                Settings.setUniqueDeviceId(Global.DeviceUniqueID);
            }
            Global.version = Utils.getVersionName(this);
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
            ImageLoaderConfiguration configuration = new ImageLoaderConfiguration
                    .Builder(getApplicationContext())
                    .defaultDisplayImageOptions(options)
                    .memoryCacheExtraOptions(point.x, point.y)
                    .threadPoolSize(IMAGE_LOADER_THREAD_POOL_SIZE)
                    .memoryCache(new LRULimitedMemoryCache(MEMORY_CACHE_SIZE))
                    .memoryCacheSize(MEMORY_CACHE_SIZE)
                    .diskCacheSize(DISC_CACHE_SIZE)
                    .build();
            ImageLoader.getInstance().init(configuration);
            MiPushClient.registerPush(this, Constants.MI_PUSH_APP_ID, Constants.MI_PUSH_APP_KEY);
            IWXAPI iwxapi = WXAPIFactory.createWXAPI(this, Constants.WECHAT_KEY, true);
            iwxapi.registerApp(Constants.WECHAT_KEY);
            Global.setUser(Settings.getProfile());
            MobclickAgent.updateOnlineConfig(getApplicationContext());
            StrictMode.setThreadPolicy(new ThreadPolicy.Builder().permitNetwork().build());
        }
    }

    private boolean shouldInit() {
        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<RunningAppProcessInfo> processInfos = am.getRunningAppProcesses();
        String mainProcessName = getPackageName();
        int myPid = Process.myPid();
        for (RunningAppProcessInfo info : processInfos) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                return true;
            }
        }
        return false;
    }

}
