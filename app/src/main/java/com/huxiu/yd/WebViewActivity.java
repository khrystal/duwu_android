package com.huxiu.yd;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Utils;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/18:下午9:36.
 */
public class WebViewActivity extends Activity {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.web_view)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.inject(this);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        title.setText(getIntent().getStringExtra(Constants.WEB_TITLE_KEY));
        Utils.setDefaultWebSettings(webView);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(getIntent().getStringExtra(Constants.URL_KEY));
        webView.removeJavascriptInterface("searchBoxJavaBredge_");
    }



    private class InnerWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            title.setText(view.getTitle());
        }
    }
}
