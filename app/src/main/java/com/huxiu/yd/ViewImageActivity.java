package com.huxiu.yd;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by yao on 15/9/13.
 */
public class ViewImageActivity extends Activity implements PhotoViewAttacher.OnViewTapListener, ViewPager.OnPageChangeListener {

    @InjectView(R.id.pager)
    ViewPager pager;
    @InjectView(R.id.total)
    TextView totalView;
    @InjectView(R.id.pos)
    TextView posView;

    String urls[];
    MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        ButterKnife.inject(this);
        urls = getIntent().getStringArrayExtra("urls");
        int count = 0;
        int pos = getIntent().getIntExtra("pos", 0);
        if (urls != null)
            count = urls.length;
        String s = "/";
        if (count < 10)
            s += "0";
        s += count;
        totalView.setText(s);
        mAdapter = new MyAdapter();
        pager.setAdapter(mAdapter);
        pager.addOnPageChangeListener(this);
        pager.setCurrentItem(pos);
    }

    @Override
    public void onViewTap(View view, float v, float v1) {
        finish();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        String s = "";
        if (position < 9)
            s += "0";
        s += String.valueOf(position + 1);
        posView.setText(s);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class MyAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            if (urls == null)
                return 0;
            return urls.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final String url = urls[position];
            View view = getLayoutInflater()
                    .inflate(R.layout.view_image_item, container, false);
            PhotoView imageView = (PhotoView) view.findViewById(R.id.image);
            ImageLoader.getInstance().displayImage(url, imageView);
            imageView.setOnViewTapListener(ViewImageActivity.this);
            container.addView(view);
            return view;
        }
    }
}
