package com.huxiu.yd.utils;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.huxiu.yd.net.model.BindInfo;
import com.huxiu.yd.net.model.User;

/**
 * Created by kHRYSTAL on 15/7/24:下午1:31.
 */
public class Global {

    public static int screenWidth;

    public static int preferredImageHeight;

    public static int preferredImageHeight2;

    public static String DeviceUniqueID;

    public static String platform;

    public static String version;

    public static User user;

    public static void setUser(String profile) {
        if (TextUtils.isEmpty(profile)) {
            user = null;
        } else {
            user = new Gson().fromJson(profile, User.class);
        }
    }

    public static void setUser(User user) {
        Global.user = user;
        if (user.bind != null) {
            for (BindInfo bindInfo:user.bind) {
                if ("huxiu".equals(bindInfo.type)) {
                    Settings.setHxUserId(bindInfo.openid);
                    Settings.setHxToken(bindInfo.token);
                } else if ("weibo".equals(bindInfo.type)) {
                    Settings.saveWeiboUid(bindInfo.openid);
                    Settings.setWeiboName(bindInfo.nick_name);
                } else if ("qq".equals(bindInfo.type)) {
                    Settings.saveQQUid(bindInfo.openid);
                } else if ("weixin".equals(bindInfo.type)) {
                    Settings.setWeixinUid(bindInfo.openid);
                }
            }
        }
    }

}
