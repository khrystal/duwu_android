package com.huxiu.yd.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.huxiu.yd.App;
import com.huxiu.yd.net.model.RecommendFriend;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;

/**
 * Created by yao on 15/7/23:下午5:12.
 */
public class Settings {

    public static final String PREF_NAME = "preferences";

    static Context gContext;

    @SuppressLint("InlinedApi")
    public static SharedPreferences getPrefs() {
        if (gContext == null) {
            gContext = App.getInstance();
        }
        int code = Context.MODE_PRIVATE;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            code = Context.MODE_MULTI_PROCESS;
        }
        code |= Context.MODE_APPEND;
        return gContext.getSharedPreferences(PREF_NAME, code);
    }

    private static final String FIRST_LAUNCH_KEY = "first_launch";

    public static boolean isFirstLaunch() {
        SharedPreferences pref = getPrefs();
        boolean isFirst = pref.getBoolean(FIRST_LAUNCH_KEY, true);
        if (isFirst) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(FIRST_LAUNCH_KEY, false);
            editor.apply();
        }
        return isFirst;
    }

    private static final String SINA_WEIBO_TOKEN = "sina_weibo_token";

    private static final String SINA_WEIBO_EXPIRE_TIME = "sina_weibo_expire_time";

    private static final String QQ_TOKEN = "qq_token";

    private static final String QQ_EXPIRE_TIME = "qq_expire_time";

    private static final String WEIBO_UID_KEY = "weibo_uid";

    private static final String QQ_KEY = "qq";

    private static final String WEIBO_DISPLAY_NAME = "weibo_display_name";

    private static final String QQ_DISPLAY_NAME = "qq_display_name";

    private static final String QQ_UID_KEY = "qq_uid";

    private static final String TOKEN_KEY = "token";

    private static final String UID_KEY = "uid";

    public static void saveWeiboAccessToken(Oauth2AccessToken accessToken) {
        SharedPreferences pref = getPrefs();
        SharedPreferences.Editor editor = pref.edit();
        if (accessToken == null) {
            editor.putString(SINA_WEIBO_TOKEN, "");
            editor.putLong(SINA_WEIBO_EXPIRE_TIME, 0);
        } else {
            editor.putString(SINA_WEIBO_TOKEN, accessToken.getToken());
            editor.putLong(SINA_WEIBO_EXPIRE_TIME, accessToken.getExpiresTime());
        }
        editor.apply();
    }

    public static Oauth2AccessToken readWeiboAccessToken() {
        Oauth2AccessToken token = new Oauth2AccessToken();
        SharedPreferences pref = getPrefs();
        token.setToken(pref.getString(SINA_WEIBO_TOKEN, ""));
        token.setExpiresTime(pref.getLong(SINA_WEIBO_EXPIRE_TIME, 0));
        return token;
    }

    public static void saveQQAccessToken(Oauth2AccessToken accessToken) {
        SharedPreferences pref = getPrefs();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(QQ_TOKEN, accessToken.getToken());
        editor.putLong(QQ_EXPIRE_TIME, accessToken.getExpiresTime());
        editor.apply();
    }

    public static Oauth2AccessToken readQQAccessToken() {
        Oauth2AccessToken token = new Oauth2AccessToken();
        SharedPreferences pref = getPrefs();
        token.setToken(pref.getString(QQ_TOKEN, ""));
        token.setExpiresTime(pref.getLong(QQ_EXPIRE_TIME, 0));
        return token;
    }

    public static boolean isLoggedIn() {
        String userInfo = getProfile();
        if (!TextUtils.isEmpty(userInfo)) {
            return true;
        }
        return false;
    }

    public static String getToken() {
        return getPrefs().getString(TOKEN_KEY, "");
    }

    public static String getUid() {
        return getPrefs().getString(UID_KEY, "");
    }

    public static void saveWeiboUid(String id) {
        SharedPreferences pref = getPrefs();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(WEIBO_UID_KEY, id);
        editor.apply();
    }

    public static String getWeiboUid() {
        return getPrefs().getString(
                WEIBO_UID_KEY, "");
    }

    public static void saveQQLogInfo(String info) {
        SharedPreferences pref = getPrefs();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(QQ_KEY, info);
        editor.apply();
    }

    public static String getQQLogInfo() {
        return getPrefs().getString(
                QQ_KEY, "");
    }

    public static String getWeiboName() {
        return getPrefs().getString(
                WEIBO_DISPLAY_NAME, null);
    }

    public static void setWeiboName(String text) {
        SharedPreferences pref = getPrefs();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(WEIBO_DISPLAY_NAME, text);
        editor.apply();
    }

    public static String getQQName() {
        return getPrefs().getString(
                QQ_DISPLAY_NAME, null);
    }

    public static void setQQName(String text) {
        SharedPreferences pref = getPrefs();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(QQ_DISPLAY_NAME, text);
        editor.apply();
    }

    public static void saveQQUid(String id) {
        SharedPreferences pref = getPrefs();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(QQ_UID_KEY, id);
        editor.apply();
    }

    public static String getQQUid() {
        return getPrefs().getString(QQ_UID_KEY, "");
    }

    public static void clearLogInInfo() {
        SharedPreferences preferences = getPrefs();
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(PROFILE);
        editor.apply();
    }

    public static void clearQQLogInInfo() {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.remove(QQ_DISPLAY_NAME);
        editor.remove(QQ_EXPIRE_TIME);
        editor.remove(QQ_KEY);
        editor.remove(QQ_TOKEN);
        editor.remove(QQ_UID_KEY);
        editor.apply();
    }

    public static void clearSinaLogInInfo() {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.remove(SINA_WEIBO_EXPIRE_TIME);
        editor.remove(SINA_WEIBO_TOKEN);
        editor.remove(WEIBO_DISPLAY_NAME);
        editor.remove(WEIBO_UID_KEY);
        editor.apply();
    }

    public static void logOut() {
        clearLogInInfo();
        clearQQLogInInfo();
        clearSinaLogInInfo();
        getPrefs().edit().clear().apply();
    }

    public static final String PROFILE = "profile";

    public static void saveProfile(String profile) {
        getPrefs().edit().putString(PROFILE, profile).apply();
    }

    public static String getProfile() {
        return getPrefs().getString(PROFILE, "");
    }

    private static final String UNIQUE_DEVICE_ID = "unique_device_id";

    public static String getUniqueDeviceId() {
        return getPrefs().getString(UNIQUE_DEVICE_ID, null);
    }

    public static void setUniqueDeviceId(String uid) {
        getPrefs().edit().putString(UNIQUE_DEVICE_ID, uid).apply();
    }

    public static void saveSignature(String s) {
        getPrefs().edit().putString("signature", s).commit();
    }

    public static String getSignature() {
        return getPrefs().getString("signature", "");
    }

    public static boolean isWeiboLoggedIn() {
        return !TextUtils.isEmpty(Settings.getWeiboUid());
    }

    public static boolean isQQLoggedIn() {
        return !TextUtils.isEmpty(Settings.getQQUid());
    }

    public static final String WEIXIN_UID = "weixin_uid";

    public static final String WEIXIN_TOKEN = "weixin_token";

    public static void setWeixinUid(String uid) {
        getPrefs().edit().putString(WEIXIN_UID, uid).commit();
    }

    public static void setWeixinToken(String token) {
        getPrefs().edit().putString(WEIXIN_TOKEN, token).commit();
    }

    public static String getWeixinUid() {
        return getPrefs().getString(WEIXIN_UID, "");
    }

    public static String getWeixinToken() {
        return getPrefs().getString(WEIXIN_TOKEN, "");
    }

    public static boolean isWeixinLoggedIn() {
        return !TextUtils.isEmpty(Settings.getWeixinUid());
    }

    public static final String HX_TOKEN = "hx_token";

    public static final String HX_USER_ID = "hx_user_id";

    public static String getHxToken() {
        return getPrefs().getString(HX_TOKEN, "");
    }

    public static void setHxToken(String token) {
        getPrefs().edit().putString(HX_TOKEN, token).apply();
    }

    public static void setHxUserId(String userId) {
        getPrefs().edit().putString(HX_USER_ID, userId).apply();
    }

    public static String getHxUserId() {
        return getPrefs().getString(HX_USER_ID, "");
    }

    public static boolean isHuxiuLoggedIn() {
        return !TextUtils.isEmpty(getHxToken()) && !TextUtils.isEmpty(getHxUserId());
    }

    private static final String MI_ID = "mi_push_id";

    public static void setMiId(String id) {
        getPrefs().edit().putString(MI_ID, id).apply();
    }

    public static String getMiId() {
        return getPrefs().getString(MI_ID, null);
    }

    private static final String RECOMMEND_KEY = "recommend";

    public static void setRecommend(RecommendFriend data) {
        getPrefs().edit().putString(RECOMMEND_KEY, new Gson().toJson(data)).apply();
    }

    public static RecommendFriend getRecommend() {
        String data = getPrefs().getString(RECOMMEND_KEY, null);
        if (data != null) {
            RecommendFriend rf = new Gson().fromJson(data, RecommendFriend.class);
            return rf;
        }
        return null;
    }
}
