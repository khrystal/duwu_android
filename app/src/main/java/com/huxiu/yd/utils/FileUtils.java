package com.huxiu.yd.utils;

import com.huxiu.yd.App;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by tian on 14/10/20:下午11:46.
 */
public class FileUtils {

    public static File getCacheDir() {
        Context context = App.getInstance();
        File cacheDir = context.getExternalCacheDir();
        if (cacheDir == null) {
            File sdDir = Environment.getExternalStorageDirectory();
            cacheDir = new File(sdDir, "/android/data/" + context.getPackageName()
                    + "/cache");
        }
        return cacheDir;

    }

    public static long folderSize(File directory) {
        long length = 0;
        if (directory == null) {
            return 0;
        }
        File[] files = directory.listFiles();
        if (files == null) {
            return 0;
        }
        for (File file : files) {
            if (file == null) {
                continue;
            }
            if (file.isFile()) {
                length += file.length();
            } else {
                length += folderSize(file);
            }
        }
        return length;
    }


    public static boolean hasSDCard() {
        String t = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(t);
    }

    private static final String SPLASH_FILE = "splash";

    public static File getSplashFile() {
        return new File(App.getInstance().getCacheDir(), SPLASH_FILE);
    }

    public static BitmapDrawable getSplashImage() {
        return new BitmapDrawable(App.getInstance().getResources(),
                getSplashFile().getAbsolutePath());
    }

    private static final String AVATAR_FILE = "avatar.jpg";

    public static File getAvatarFile() {
        return new File(getCacheDir(), AVATAR_FILE);
    }

    public static void writeAvatarFile(byte[] data) {
        FileOutputStream fos;
        try {
            File dir = App.getInstance().getCacheDir();
            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    throw new IOException("mkdirs failed: " + dir.getAbsolutePath());
                }
            }
            File f = getAvatarFile();
            fos = new FileOutputStream(f);
            fos.write(data, 0, data.length);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static byte[] inputStreamToByteArray(InputStream is) {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[16384];

        try {
            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
        } catch (Exception e) {
            Log.e("FileUtils", Log.getStackTraceString(e));
        }
        return buffer.toByteArray();
    }

    public static void copyFile(File sourceFile, File targetFile) throws IOException {
        BufferedInputStream inBuff = null;
        BufferedOutputStream outBuff = null;
        try {
            inBuff = new BufferedInputStream(new FileInputStream(sourceFile));
            outBuff = new BufferedOutputStream(new FileOutputStream(targetFile));
            byte[] b = new byte[1024 * 10];
            int len;
            while ((len = inBuff.read(b)) != -1) {
                outBuff.write(b, 0, len);
            }
            outBuff.flush();
        } finally {
            if (inBuff != null) {
                inBuff.close();
            }
            if (outBuff != null) {
                outBuff.close();
            }
        }
    }

    private static final long K = 1024;

    private static final long M = K * K;

    private static final long G = M * K;

    private static final long T = G * K;

    public static String getFileSizeString(long size) {
        final long[] dividers = new long[]{T, G, M, K, 1};
        final String[] units = new String[]{"TB", "GB", "MB", "KB", "B"};
        if (size < 1) {
            return "0KB";
        }
        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            final long divider = dividers[i];
            if (size >= divider) {
                result = format(size, divider, units[i]);
                break;
            }
        }
        return result;
    }

    private static String format(final long value,
            final long divider,
            final String unit) {
        final double result =
                divider > 1 ? (double) value / (double) divider : (double) value;
        return String.format("%.1f %s", result, unit);
    }

}
