package com.huxiu.yd;

import com.android.volley.Request.Method;
import com.android.volley.Response.Listener;
import com.huxiu.yd.net.GsonRequest;
import com.huxiu.yd.net.NetworkConstants;
import com.huxiu.yd.net.responses.BaseResponse;
import com.huxiu.yd.utils.Utils;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/7/29:下午9:21.
 */
public class FeedbackActivity extends BaseActivity implements OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.left_text)
    TextView leftText;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.right_image)
    ImageView rightImage;

    @InjectView(R.id.right_text)
    TextView rightText;

    @InjectView(R.id.edit)
    EditText edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        leftText.setVisibility(View.GONE);
        title.setText(R.string.feedback);
        rightImage.setVisibility(View.GONE);
        rightText.setText(R.string.send);
        rightText.setTextColor(getResources().getColor(R.color.white));
        rightText.setVisibility(View.VISIBLE);
        rightText.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                if (!TextUtils.isEmpty(edit.getText().toString())) {
                    new Builder(this).setTitle(R.string.content_not_empty)
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    }).setNegativeButton(android.R.string.cancel, null).show();
                } else {
                    finish();
                }
                break;
            case R.id.right_text:
                submitFeedback();
                break;
        }
    }

    private void submitFeedback() {
        String url = NetworkConstants.MEMBER_URL;
        Map<String, String> params = new LinkedHashMap<>();
        params.put("act", "add_suggest");
        params.put("content", edit.getText().toString());
        GsonRequest<BaseResponse> request = new GsonRequest<>(url, Method.POST, BaseResponse.class,
                true, params, new Listener<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                dismissProgress();
                Utils.showToast(R.string.submit_feedback_success);
                finish();
            }
        }, mErrorListener);
        showProgress();
        mQueue.add(request);
//        mQueue.start();
    }
}
