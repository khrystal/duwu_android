package com.huxiu.yd;

import com.huxiu.yd.fragments.MyAttentionMassiveTestFragment;
import com.huxiu.yd.fragments.SpareFragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MyAttentionsActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.massive_test)
    View massiveTest;

    @InjectView(R.id.spare)
    View spare;

    @InjectView(R.id.fragments)
    FrameLayout fragment;

    private String[] tabs = new String[2];

    private int currentFragmentIndex;

    private Fragment currentFragment;

    private Map<Integer, Fragment> fragments = new HashMap<>(2);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_attentions);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        title.setText(R.string.my_attentions);
        tabs[0] = getString(R.string.massive_test);
        tabs[1] = getString(R.string.spare);
        massiveTest.setOnClickListener(this);
        spare.setOnClickListener(this);
        massiveTest.setSelected(true);
        setupFragments(savedInstanceState);
    }

    private void setupFragments(Bundle savedInstanceState) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("attention", true);
        if (fragments.size() != 2) {
            fragments.put(R.id.massive_test,
                    Fragment.instantiate(this, MyAttentionMassiveTestFragment.class.getName(),
                            bundle));
            fragments.put(R.id.spare,
                    Fragment.instantiate(this, SpareFragment.class.getName(), bundle));
            currentFragment = fragments.get(R.id.massive_test);
            currentFragmentIndex = R.id.massive_test;
        }
        if (savedInstanceState == null) {
            currentFragmentIndex = R.id.massive_test;
        } else {
            currentFragmentIndex = savedInstanceState.getInt("tab");
            while (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            }
        }
        Fragment fragment = fragments.get(currentFragmentIndex);
        getFragmentManager().beginTransaction().add(R.id.fragments, fragment).show(fragment)
                .commitAllowingStateLoss();
    }

    private void switchFragment(View v) {
        if (currentFragmentIndex == v.getId()) {
            return;
        }
        spare.setSelected(false);
        massiveTest.setSelected(false);
        v.setSelected(true);

        Fragment fragment = fragments.get(v.getId());
        if (fragment.isAdded()) {
            getFragmentManager().beginTransaction().hide(currentFragment).show(fragment)
                    .commit();
        } else {
            getFragmentManager().beginTransaction().hide(currentFragment)
                    .add(R.id.fragments, fragment, String.valueOf(v.getId())).commit();
        }
        currentFragmentIndex = v.getId();
        currentFragment = fragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.massive_test:
                switchFragment(v);
                break;
            case R.id.spare:
                switchFragment(v);
                break;
        }
    }
}
