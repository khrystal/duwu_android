package com.huxiu.yd;

import com.huxiu.yd.utils.Settings;
import com.huxiu.yd.utils.Utils;
import com.tencent.mm.sdk.modelmsg.SendAuth.Req;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/3:下午8:44.
 */
public class BindAccountsActivity extends AuthenticatorActivity implements View.OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.left_text)
    TextView leftText;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.weibo_image)
    ImageView weiboImage;

    @InjectView(R.id.bind_weibo)
    TextView bindWeibo;

    @InjectView(R.id.weixin_image)
    ImageView weixinImage;

    @InjectView(R.id.bind_weixin)
    TextView bindWeixin;

    @InjectView(R.id.qq_image)
    ImageView qqImage;

    @InjectView(R.id.bind_qq)
    TextView bindQq;

    @InjectView(R.id.weibo_button)
    TextView weiboButton;

    @InjectView(R.id.weixin_button)
    TextView weixinButton;

    @InjectView(R.id.qq_button)
    TextView qqButton;

    @InjectView(R.id.huxiu_image)
    ImageView huxiuImage;

    @InjectView(R.id.huxiu_button)
    TextView huxiuButton;

    @InjectView(R.id.bind_huxiu)
    TextView bindHuxiu;

    public static final int REQUEST_BIND_HUXIU = 0x10001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_accounts);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        leftText.setVisibility(View.GONE);
        title.setText(R.string.bind_accounts);
        bindWeibo.setOnClickListener(this);
        bindWeixin.setOnClickListener(this);
        bindQq.setOnClickListener(this);
        bindHuxiu.setOnClickListener(this);
        updateUI();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.bind_weibo:
                if (Settings.isWeiboLoggedIn()) {
                    unbind("weibo");
                } else {
                    mSsoHandler.authorize(this);
                }
                break;
            case R.id.bind_qq:
                if (Settings.isQQLoggedIn()) {
                    unbind("qq");
                } else {
                    mTencent.login(this, "all", this);
                }
                break;
            case R.id.bind_weixin:
                if (Settings.isWeixinLoggedIn()) {
                    unbind("weixin");
                } else {
                    if (mWechatApi.isWXAppInstalled()) {
                        Req req = new Req();
                        req.scope = "snsapi_userinfo";
                        mWechatApi.sendReq(req);
                    } else {
                        Utils.showToast(R.string.weixin_not_installed);
                    }
                }
                break;
            case R.id.bind_huxiu:
                if (Settings.isHuxiuLoggedIn()) {
                    unbind("huxiu");
                } else {
                    Intent intent = new Intent(this, HuxiuLogInActivity.class);
                    intent.putExtra("Bind", true);
                    startActivityForResult(intent, REQUEST_BIND_HUXIU);
                }
                break;
        }
    }

    @Override
    protected void updateUI() {
        weiboImage.setImageResource(!Settings.isWeiboLoggedIn() ?
                R.drawable.weibo_gray : R.drawable.log_in_weibo_selected);
        bindWeibo.setText(Settings.isWeiboLoggedIn() ? R.string.unbind : R.string.bind);
        bindWeibo.setSelected(Settings.isWeiboLoggedIn());
        qqImage.setImageResource(
                !Settings.isQQLoggedIn() ? R.drawable.qq_gray : R.drawable.log_in_qq_selected);
        bindQq.setText(Settings.isQQLoggedIn() ? R.string.unbind : R.string.bind);
        bindQq.setSelected(Settings.isQQLoggedIn());
        weixinImage.setImageResource(!Settings.isWeixinLoggedIn() ?
                R.drawable.weixin_gray : R.drawable.log_in_weixin_selected);
        bindWeixin.setText(Settings.isWeixinLoggedIn() ? R.string.unbind : R.string.bind);
        bindWeixin.setSelected(Settings.isWeixinLoggedIn());
        huxiuImage.setImageResource(!Settings.isHuxiuLoggedIn() ? R.drawable.huxiu_gray : R
                .drawable.log_in_huxiu_selected);
        bindHuxiu.setText(Settings.isHuxiuLoggedIn() ? R.string.unbind : R.string.bind);
        bindHuxiu.setSelected(Settings.isHuxiuLoggedIn());
    }

    private void unbind(String type) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("act", "user_bind");
        params.put("bind_type", type);
//        params.put("token", accessToken.getToken());
//        params.put("openid", accessToken.getUid());
        if (type.equals("weibo")) {
            params.put("openid", Settings.getWeiboUid());
        } else if (type.equals("huxiu")) {
            params.put("openid", Settings.getHxUserId());
        } else if (type.equals("qq")) {
            params.put("openid",Settings.getQQUid());
        } else if (type.equals("weixin")) {
            params.put("openid",Settings.getWeixinUid());
        }
        params.put("operation_type", "unbind");
        doBind(params);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_BIND_HUXIU && resultCode == RESULT_OK) {
            String openid = data.getStringExtra("openid");
            String token = data.getStringExtra("token");
            Settings.setHxToken(token);
            Settings.setHxUserId(openid);
            Map<String, String> params = new LinkedHashMap<>();
            params.put("act", "user_bind");
            params.put("bind_type", "huxiu");
            params.put("token", token);
            params.put("openid", openid);
            params.put("operation_type", "bind");
            doBind(params);
        }
    }
}
