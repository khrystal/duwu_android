package com.huxiu.yd;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.Response.Listener;
import com.huxiu.yd.net.GsonRequest;
import com.huxiu.yd.net.NetworkConstants;
import com.huxiu.yd.net.responses.ProfileResponse;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Global;
import com.huxiu.yd.utils.Settings;
import com.huxiu.yd.utils.Utils;

import java.util.LinkedHashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/7/23:上午11:21.
 */
public class HuxiuLogInActivity extends AuthenticatorActivity implements OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.right_image)
    ImageView rightImage;

    @InjectView(R.id.right_text)
    TextView rightText;

    @InjectView(R.id.username_edit)
    EditText usernameEdit;

    @InjectView(R.id.password_edit)
    EditText passwordEdit;

    @InjectView(R.id.log_in_button)
    Button logInButton;

    private static final boolean DEBUG = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_huxiu_login);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        title.setText(R.string.huxiu_login);
        rightImage.setVisibility(View.GONE);
        logInButton.setOnClickListener(this);
        if (DEBUG) {
            usernameEdit.setText("firstfan@gmail.com");
            passwordEdit.setText("123456");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.log_in_button:
                if (inputValid()) {
                    logIn("huxiu");
                }
                break;
        }
    }

    private boolean inputValid() {
        if (TextUtils.isEmpty(usernameEdit.getText().toString())) {
            Utils.showToast(R.string.invalid_username);
            return false;
        }
        if (TextUtils.isEmpty(passwordEdit.getText().toString())) {
            Utils.showToast(R.string.invalid_password);
            return false;
        }
        return true;
    }

    protected void logIn(String type) {
        String url = NetworkConstants.MEMBER_URL;
        Map<String, String> params = new LinkedHashMap<>();
        params.put("username", usernameEdit.getText().toString());
        params.put("password", passwordEdit.getText().toString());
//        LogUtils.d("password is " + Utils.getEncryptedPassword(passwordEdit.getText().toString()));
        params.put("act", "login");
        params.put("login_type", type);
        GsonRequest<ProfileResponse> request = new GsonRequest<>(url, Method.POST,
                ProfileResponse.class, false, params, new Listener<ProfileResponse>() {
            @Override
            public void onResponse(ProfileResponse response) {
                dismissProgress();
                if (response.isSuccess()) {
                    if (forBind) {
                        Intent data = new Intent();
                        data.putExtra("openid", response.data.openid);
                        data.putExtra("token", response.data.token);
                        setResult(RESULT_OK, data);
                        finish();
                        return;
                    }
                    Settings.setHxUserId(response.data.openid);
                    Settings.saveProfile(response.data.toString());
                    Global.setUser(response.data);
                    Utils.showToast(R.string.log_in_success);
                    LocalBroadcastManager.getInstance(App.getInstance()).sendBroadcast(new Intent(
                            Constants.INTENT_USER_LOGGED_IN));
                    setResult(RESULT_OK);
                    finish();
                }
            }
        }, mErrorListener);
        mQueue.add(request);
//        mQueue.start();
        showProgress();
    }

    private static final String TAG = HuxiuLogInActivity.class.getCanonicalName();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}