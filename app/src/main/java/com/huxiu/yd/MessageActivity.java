package com.huxiu.yd;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.huxiu.yd.fragments.MessageListFragment;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/26.
 */
public class MessageActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.tab_private_msg)
    FrameLayout tabPrivateMsg;

    @InjectView(R.id.tab_notification)
    FrameLayout tabNotification;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.viewpager)
    ViewPager mViewPager;

    @InjectView(R.id.back)
    ImageView back;

    Fragment privateMsgFragment;
    Fragment notificationFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.inject(this);
        tabPrivateMsg.setOnClickListener(this);
        tabNotification.setOnClickListener(this);
        privateMsgFragment = MessageListFragment.newInstance(MessageListFragment.MSG_TYPE_PRIVATE);
        notificationFragment = MessageListFragment.newInstance(MessageListFragment.MSG_TYPE_NOTIFICATION);
        MyPagerAdapter adapter = new MyPagerAdapter(getFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabPrivateMsg.setSelected(position == 0);
                tabNotification.setSelected(position == 1);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tabPrivateMsg.setSelected(true);
        title.setText(R.string.activity_message_label);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab_private_msg:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.tab_notification:
                mViewPager.setCurrentItem(1);
                break;
        }
    }

    class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return privateMsgFragment;
                case 1:
                    return notificationFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
