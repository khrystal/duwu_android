package com.huxiu.yd.fragments;

import com.google.gson.Gson;

import com.huxiu.yd.LogInActivity;
import com.huxiu.yd.MessageActivity;
import com.huxiu.yd.MyAttentionsActivity;
import com.huxiu.yd.MyMassiveTestActivity;
import com.huxiu.yd.MySpareActivity;
import com.huxiu.yd.R;
import com.huxiu.yd.SettingsActivity;
import com.huxiu.yd.net.model.User;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Global;
import com.huxiu.yd.utils.Settings;
import com.huxiu.yd.utils.Utils;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by kHRYSTAL on 15/9/18:下午2:45.
 */
public class ProfileFragment extends Fragment implements OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.left_text)
    TextView leftText;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.right_image)
    ImageView rightImage;

    @InjectView(R.id.right_text)
    TextView rightText;

    @InjectView(R.id.avatar)
    RoundedImageView avatar;

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.sign)
    TextView sign;

    @InjectView(R.id.my_attentions)
    TextView myAttentions;

    @InjectView(R.id.my_massive_tests)
    TextView myMassiveTests;

    @InjectView(R.id.my_spare)
    TextView mySpare;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.inject(this, view);
        initView();
        IntentFilter intent = new IntentFilter(Constants.INTENT_USER_LOGGED_IN);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver, intent);
        bindProfileData();
        return view;
    }

    private void initView() {
        avatar.setOnClickListener(this);
        back.setImageResource(R.drawable.ic_message);
        back.setOnClickListener(this);
        title.setText(R.string.me);
        leftText.setVisibility(View.GONE);
        rightText.setVisibility(View.GONE);
        rightImage.setVisibility(View.VISIBLE);
        rightImage.setImageResource(R.drawable.ic_settings);
        rightImage.setOnClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    public static final int LOG_IN_INTENT = 1;

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.avatar:
                if (!Settings.isLoggedIn()) {
                    intent = new Intent(getActivity(), LogInActivity.class);
                    startActivityForResult(intent, LOG_IN_INTENT);
                }
                break;
            case R.id.back:
                if (!Settings.isLoggedIn()) {
                    intent = new Intent(getActivity(), LogInActivity.class);
                    startActivityForResult(intent, LOG_IN_INTENT);
                } else {
                    startActivity(new Intent(getActivity(), MessageActivity.class));
                }
                break;
            case R.id.right_image:
                intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.my_attentions:
                if (Settings.isLoggedIn()) {
                    intent = new Intent(getActivity(), MyAttentionsActivity.class);
                    startActivity(intent);
                } else {
                    Utils.askLogIn(getActivity());
                }
                break;
            case R.id.my_massive_tests:
                if (Settings.isLoggedIn()) {
                    intent = new Intent(getActivity(), MyMassiveTestActivity.class);
                    startActivity(intent);
                } else {
                    Utils.askLogIn(getActivity());
                }
                break;
            case R.id.my_spare:
                if (Settings.isLoggedIn()) {
                    intent = new Intent(getActivity(), MySpareActivity.class);
                    startActivity(intent);
                } else {
                    Utils.askLogIn(getActivity());
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case LOG_IN_INTENT:
                //TODO
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        bindProfileData();
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Constants.INTENT_USER_LOGGED_IN)) {
                Global.user = new Gson().fromJson(Settings.getProfile(), User.class);
                bindProfileData();
            } else if (action.equals(Constants.INTENT_USER_LOGGED_OUT)) {
                Global.user = null;
                bindProfileData();
            }
        }
    };

    private void bindProfileData() {
        if (Global.user == null) {
            avatar.setImageResource(R.drawable.default_avatar);
            sign.setText("");
        } else {
            ImageLoader.getInstance().displayImage(Global.user.avatar, avatar);
            if (!TextUtils.isEmpty(Global.user.yijuhua)) {
                sign.setText(Global.user.yijuhua);
            }
            myAttentions.setOnClickListener(this);
            myMassiveTests.setOnClickListener(this);
            mySpare.setOnClickListener(this);
        }
        name.setText(Global.user == null ? getString(R.string.log_in_now) : Global.user.nickname);
    }

}
