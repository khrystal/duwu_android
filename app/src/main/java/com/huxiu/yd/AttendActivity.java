package com.huxiu.yd;

import com.android.volley.Request.Method;
import com.android.volley.Response.Listener;
import com.huxiu.yd.net.GsonRequest;
import com.huxiu.yd.net.NetworkConstants;
import com.huxiu.yd.net.responses.BaseResponse;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Utils;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/26:下午11:09.
 */
public class AttendActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.name_edit)
    EditText nameEdit;

    @InjectView(R.id.occupation_edit)
    EditText occupationEdit;

    @InjectView(R.id.contact_edit)
    EditText contactEdit;

    @InjectView(R.id.address_edit)
    EditText addressEdit;

    @InjectView(R.id.reason_edit)
    EditText reasonEdit;

    @InjectView(R.id.submit_button)
    TextView submitButton;

    private String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attend);
        id = getIntent().getStringExtra(Constants.ARTICLE_ID_KEY);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        title.setText(R.string.apply_for_massive_test);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.submit_button:
                if (isInputValid()) {
                    submitApply();
                }
                break;
        }
    }

    private void submitApply() {
        String url = NetworkConstants.TOPIC_URL;
        Map<String, String> params = new HashMap<>();
        params.put("act", "add_enroll");
        params.put("id", id);
        params.put("name", nameEdit.getText().toString());
        params.put("job", occupationEdit.getText().toString());
        params.put("contact", contactEdit.getText().toString());
        params.put("reason", reasonEdit.getText().toString());
        params.put("address", addressEdit.getText().toString());
        GsonRequest<BaseResponse> request = new GsonRequest<>(url, Method.POST, BaseResponse.class,
                true, params, new Listener<BaseResponse>() {
            @Override
            public void onResponse(BaseResponse response) {
                dismissProgress();
                if (response.isSuccess()) {
                    Utils.showToast(R.string.submit_application_success);
                    setResult(RESULT_OK);
                    finish();
                }
            }
        }, mErrorListener);
        mQueue.add(request);
//        mQueue.start();
        showProgress();
    }

    private boolean isInputValid() {
        if (TextUtils.isEmpty(nameEdit.getText().toString())) {
            Utils.showToast(R.string.pls_input_name);
            return false;
        }
        if (TextUtils.isEmpty(occupationEdit.getText().toString())) {
            Utils.showToast(R.string.pls_input_occupation);
            return false;
        }
        if (TextUtils.isEmpty(contactEdit.getText().toString())) {
            Utils.showToast(R.string.pls_input_contact_method);
            return false;
        }
        if (TextUtils.isEmpty(addressEdit.getText().toString())) {
            Utils.showToast(R.string.pls_input_address);
            return false;
        }
        if (TextUtils.isEmpty(reasonEdit.getText().toString())) {
            Utils.showToast(R.string.pls_input_reason);
            return false;
        }
        return true;
    }
}
