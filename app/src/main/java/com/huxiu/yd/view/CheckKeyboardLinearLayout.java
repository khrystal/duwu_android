package com.huxiu.yd.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by ytian on 15/6/2.
 */
public class CheckKeyboardLinearLayout extends LinearLayout {

    public interface OnKeyboardShownListener {
        void onKeyboardShown();

        void onKeyboardHidden();
    }

    private int max_height = 0;
    private OnKeyboardShownListener mListener = null;

    public CheckKeyboardLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CheckKeyboardLinearLayout(Context context) {
        super(context);
    }

    public CheckKeyboardLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setListener(OnKeyboardShownListener l) {
        mListener = l;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (h == 0) {
            return;
        }
        if (h > max_height) {
            max_height = h;
        } else if (h < max_height * 0.9 && mListener != null) {
            mListener.onKeyboardShown();
        } else if (h == max_height && mListener != null) {
            mListener.onKeyboardHidden();
        }
    }

}
