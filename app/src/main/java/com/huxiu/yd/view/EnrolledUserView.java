package com.huxiu.yd.view;

import com.huxiu.yd.EnrolledUserListActivity;
import com.huxiu.yd.R;
import com.huxiu.yd.utils.LogUtils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by ytian on 15/6/30.
 */
public class EnrolledUserView extends LinearLayout implements View.OnClickListener {
    AvatarView avatar;

    TextView name;

    private boolean isMore = false;

    public EnrolledUserView(Context context) {
        super(context);
        init(context);
    }

    public EnrolledUserView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public EnrolledUserView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EnrolledUserView(Context context, AttributeSet attrs, int defStyleAttr, int
            defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.enrolled_user_view, this);
        avatar = (AvatarView) findViewById(R.id.avatar);
        name = (TextView) findViewById(R.id.name);
        setOnClickListener(this);
    }

    private int id;
    public void setMoreData(int id) {
        avatar.setImageResource(R.drawable.enrolled_more);
        avatar.setBorderWidth(0.0f);
        name.setText(R.string.more);
        isMore = true;
        this.id = id;
    }

    public void setData(String userId, String avatarUrl, String username) {
        LogUtils.d("setData, user id " + userId + ", avatarUrl " + avatarUrl + ", username " + username);
        avatar.setUserInfo(userId, avatarUrl, username);
        name.setText(username);
    }

    @Override
    public void onClick(View v) {
        if (isMore) {
            Intent intent = new Intent(getContext(), EnrolledUserListActivity.class);
            intent.putExtra("topic_id", id);
            getContext().startActivity(intent);
        } else
            avatar.performClick();
    }
}
