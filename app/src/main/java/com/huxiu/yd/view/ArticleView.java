package com.huxiu.yd.view;

import com.huxiu.yd.DiscoveryDetailActivity;
import com.huxiu.yd.R;
import com.huxiu.yd.net.model.ArticleItem;
import com.huxiu.yd.net.model.DiscoveryItem;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.ImageUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by kHRYSTAL on 15/8/12.
 */
public class ArticleView extends LinearLayout implements View.OnClickListener {
    ImageView image;

    TextView title;

    View divider;

    public ArticleView(Context context) {
        super(context);
        init(context);
    }

    public ArticleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ArticleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ArticleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.article_grid_item, this);
        image = (ImageView) findViewById(R.id.image);
        title = (TextView) findViewById(R.id.text);
        divider = findViewById(R.id.divider);
        setOnClickListener(this);
    }

    private ArticleItem mArticleItem;

    public void setArticle(ArticleItem item, int width, int height) {
        mArticleItem = item;
        title.setText(item.title);
        divider.setBackgroundColor(item.getDividerColor());
        LinearLayout.LayoutParams params2 = (LayoutParams) image.getLayoutParams();
        params2.width = width;
        params2.height = height;
        image.setLayoutParams(params2);
        ImageLoader.getInstance().displayImage(item.image + "!" + height + "x" + width, image,
                ImageUtils.getProductDisplayOption2(item.getProductPlaceHolder()));
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getContext(), DiscoveryDetailActivity.class);
        DiscoveryItem item = new DiscoveryItem();
        item.id = Integer.parseInt(mArticleItem.article_id);
        item.image = mArticleItem.image;
        item.color = mArticleItem.color;
        item.color_type = mArticleItem.color_type;
        item.title = mArticleItem.title;
        intent.putExtra(Constants.DISCOVERY_ITEM_KEY, item.toString());
        getContext().startActivity(intent);
    }
}
