package com.huxiu.yd.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.huxiu.yd.R;
import com.huxiu.yd.net.model.Province;
import com.huxiu.yd.net.model.Province.City;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kHRYSTAL on 15/8/12:上午10:06.
 */
public class BusinessDistrictView extends LinearLayout {

    public BusinessDistrictView(Context context) {
        super(context);
        init(context);
    }

    public BusinessDistrictView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BusinessDistrictView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private ListView districtList;

    private ListView businessDistrictList;

    private int currentProvinceIndex = 0;

    private int currentCityIndex = 0;

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.business_district_view, this);
        districtList = (ListView) findViewById(R.id.district_list);
        businessDistrictList = (ListView) findViewById(R.id.business_district_list);
    }

    private List<Province> provinces = new ArrayList<>();

    private ProvinceAdapter mProvinceAdapter;

    private CityAdapter mCityAdapter;

    private OnCitySelectedListener mListener;

    public void initData(List<Province> data) {
        currentProvinceIndex = 0;
        provinces.addAll(data);
        mProvinceAdapter = new ProvinceAdapter();
        mCityAdapter = new CityAdapter();
        districtList.setAdapter(mProvinceAdapter);
        districtList.setOnItemClickListener(mDistrictItemClickListener);
        businessDistrictList.setAdapter(mCityAdapter);
        businessDistrictList.setOnItemClickListener(mBusinessDistrictItemClickListener);
    }

    public void setListener(OnCitySelectedListener listener) {
        mListener = listener;
    }

    private AdapterView.OnItemClickListener mDistrictItemClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            currentProvinceIndex = position;
            currentCityIndex = 0;
            mProvinceAdapter.notifyDataSetChanged();
            mCityAdapter.notifyDataSetChanged();
        }
    };

    private AdapterView.OnItemClickListener mBusinessDistrictItemClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            currentCityIndex = position;
            mCityAdapter.notifyDataSetChanged();
            if (mListener != null) {
                Province district = provinces.get(currentProvinceIndex);
                String cityName = district.city[currentCityIndex].city;
                mListener.onCitySelected(currentProvinceIndex, currentCityIndex, cityName);
            }
        }
    };

    public interface OnCitySelectedListener {

        void onCitySelected(int provinceIndex, int cityIndex, String cityName);
    }

    private class ProvinceAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return provinces.size();
        }

        @Override
        public Province getItem(int position) {
            return provinces.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v;
            if (position == currentProvinceIndex) {
                v = LayoutInflater.from(getContext())
                        .inflate(R.layout.district_list_item_selected, parent, false);
            } else {
                v = LayoutInflater.from(getContext())
                        .inflate(R.layout.district_list_item, parent, false);
            }
            TextView textView = (TextView) v.findViewById(R.id.district);
            textView.setText(getItem(position).province);
            return v;
        }
    }

    private class CityAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            if (currentProvinceIndex == -1) {
                return 0;
            } else {
                Province district = provinces.get(currentProvinceIndex);
                return district.city.length;
            }
        }

        @Override
        public City getItem(int position) {
            if (currentProvinceIndex == -1) {
                return null;
            } else {
                Province district = provinces.get(currentProvinceIndex);
                return district.city[position];
            }
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = LayoutInflater.from(getContext())
                        .inflate(R.layout.city_filter_list_item, parent, false);
            }
            TextView textView = (TextView) v.findViewById(R.id.district);
            textView.setText(getItem(position).city);
            textView.setTextColor(getResources()
                    .getColor(position == currentCityIndex ? R.color.button_green : R.color.gray5));
            return v;
        }
    }
}
