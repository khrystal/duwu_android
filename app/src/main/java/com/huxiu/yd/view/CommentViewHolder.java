package com.huxiu.yd.view;

import com.huxiu.yd.R;
import com.makeramen.roundedimageview.RoundedImageView;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * This class contains all butterknife-injected Views & Layouts from layout file
 * 'comment_list_item.xml'
 * for easy to all layout elements.
 *
 * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers
 *         (http://github.com/avast)
 */
public class CommentViewHolder {

    @InjectView(R.id.container)
    public View container;

    @InjectView(R.id.comment_header)
    public TextView commentHeader;

    @InjectView(R.id.comment_header_container)
    public View header;

    @InjectView(R.id.avatar)
    public AvatarView avatar;

    @InjectView(R.id.name)
    public TextView name;

    @InjectView(R.id.time)
    public TextView time;

    @InjectView(R.id.content)
    public TextView content;

    @InjectView(R.id.bottom_divider)
    public View bottomDivider;

    @InjectView(R.id.dianping_count)
    public TextView dianpingCount;

    public CommentViewHolder(View view) {
        ButterKnife.inject(this, view);
    }
}
