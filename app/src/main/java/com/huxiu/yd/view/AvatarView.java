package com.huxiu.yd.view;

import com.huxiu.yd.UserProfileActivity;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Global;
import com.huxiu.yd.utils.ImageUtils;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by kHRYSTAL on 15/8/12:上午10:46.
 */
public class AvatarView extends RoundedImageView implements View.OnClickListener {

    public AvatarView(Context context) {
        super(context);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AvatarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setBorderWidth(0.3333f);
        setBorderColor(0xff333333);
    }

    private String userId, avatar, username;

    public void setUserInfo(String userId, String avatar, String username) {
        this.userId = userId;
        this.avatar = avatar;
        this.username = username;
        ImageLoader.getInstance().displayImage(avatar, this, ImageUtils.getAvatarDisplayOption());
        setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(userId) || (Global.user != null && Global.user.user_id
                .equals(userId))) {
            return;
        }
        Intent intent = new Intent(getContext(), UserProfileActivity.class);
        intent.putExtra(Constants.USER_ID_KEY, userId);
        intent.putExtra("avatar", avatar);
        intent.putExtra("username", username);
        getContext().startActivity(intent);
    }
}
