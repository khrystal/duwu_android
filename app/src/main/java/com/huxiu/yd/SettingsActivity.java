package com.huxiu.yd;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.huxiu.yd.net.model.RecommendFriend;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.FileUtils;
import com.huxiu.yd.utils.Global;
import com.huxiu.yd.utils.Settings;
import com.huxiu.yd.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/26:下午11:42.
 */
public class SettingsActivity extends Activity implements OnClickListener {

    @InjectView(R.id.back)
    ImageView back;

    @InjectView(R.id.left_text)
    TextView leftText;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.right_image)
    ImageView rightImage;

    @InjectView(R.id.right_text)
    TextView rightText;

    @InjectView(R.id.modify_profile)
    TextView modifyProfile;

    @InjectView(R.id.bind_accounts)
    TextView bindAccounts;

    @InjectView(R.id.feedback)
    TextView feedback;

    @InjectView(R.id.about_us)
    TextView aboutUs;

    @InjectView(R.id.recommend_to_friends)
    TextView recommendToFriends;

    @InjectView(R.id.clear_cache)
    TextView clearCache;

    @InjectView(R.id.cache_size)
    TextView cacheSize;

    @InjectView(R.id.log_out)
    TextView logOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        rightImage.setVisibility(View.GONE);
        rightText.setVisibility(View.GONE);
        leftText.setVisibility(View.GONE);
        title.setText(R.string.settings);
        modifyProfile.setOnClickListener(this);
        bindAccounts.setOnClickListener(this);
        feedback.setOnClickListener(this);
        aboutUs.setOnClickListener(this);
        recommendToFriends.setOnClickListener(this);
        clearCache.setOnClickListener(this);
        logOut.setOnClickListener(this);
        logOut.setVisibility(Settings.isLoggedIn() ? View.VISIBLE : View.GONE);
        getCacheSize();
    }

    private void getCacheSize() {
        File f = FileUtils.getCacheDir();
        long size = FileUtils.folderSize(f);
        if (size < 60000) {
            size = 0;
        }
        cacheSize.setText(FileUtils.getFileSizeString(size));
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.modify_profile:
                if (Settings.isLoggedIn()) {
                    intent = new Intent(this, ViewProfileActivity.class);
                    startActivity(intent);
                } else {
                    Utils.askLogIn(this);
                }
                break;
            case R.id.bind_accounts:
                if (Settings.isLoggedIn()) {
                    intent = new Intent(this, BindAccountsActivity.class);
                    startActivity(intent);
                } else {
                    Utils.askLogIn(this);
                }
                break;
            case R.id.clear_cache:
                clearCache();
                break;
            case R.id.feedback:
                intent = new Intent(this, FeedbackActivity.class);
                startActivity(intent);
                break;
            case R.id.recommend_to_friends: {
                Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
                RecommendFriend rf = Settings.getRecommend();
                if (rf != null) {
                    Utils.launchShareActivity(this, rf.rf_title,
                            rf.rf_url + "?rf_shareid=" + rf.rf_shareId + "&rf_share_type=" + rf.rf_share_type,
                            "topic", rf.rf_img,
                            null, bmp);
                } else {
                    Utils.launchShareActivity(this, "我正在使用独物, 快来加入吧", "http://duwu.me/app",
                            "topic", null,
                            null, bmp);
                }
            }
            break;
            case R.id.about_us: {
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
            }
//                intent = new Intent(this, WebViewActivity.class);
//                intent.putExtra(Constants.WEB_TITLE_KEY, getString(R.string.user_agreement));
//                intent.putExtra(Constants.URL_KEY, "file:///android_asset/user_agreement.html");
//                startActivity(intent);
            break;
            case R.id.log_out:
                if (Settings.isLoggedIn()) {
                    logOut();
                }
                break;
        }
    }

    private void logOut() {
        new AlertDialog.Builder(this).setTitle(R.string.confirm_log_out).setPositiveButton(
                android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Settings.logOut();
                        Global.user = null;
                        LocalBroadcastManager.getInstance(App.getInstance())
                                .sendBroadcast(new Intent(
                                        Constants.INTENT_USER_LOGGED_OUT));
                        finish();
                    }
                }).setNegativeButton(android.R.string.cancel, null).show();
    }

    private void clearCache() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.ask_clear_cache))
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {


                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ImageLoader.getInstance().clearDiskCache();
                                getCacheSize();
                                Toast.makeText(SettingsActivity.this,
                                        getString(R.string.clear_cache_done),
                                        Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }
                ).setNeutralButton(getString(android.R.string.cancel), null)
                .show();

    }
}