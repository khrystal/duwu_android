package com.huxiu.yd.net.responses;

import com.huxiu.yd.net.model.RecommendFriend;

/**
 * Created by kHRYSTAL on 15/9/15.
 */
public class BindPushResponse extends BaseResponse {
    public RecommendFriend recommend_friend;
}
