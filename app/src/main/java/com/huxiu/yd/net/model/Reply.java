package com.huxiu.yd.net.model;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:14.
 */
public class Reply {

    public String user_id;

    public String user_name;

    public String user_photo;

    public String content;

    public int reply_num;

    public long time;
}
