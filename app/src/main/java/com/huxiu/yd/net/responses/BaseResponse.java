package com.huxiu.yd.net.responses;

/**
 * Created by kHRYSTAL on 15/9/16:下午6:06.
 */
public class BaseResponse {

    public int result;

    public String msg;

    public int max_page;

    public int page;

    public boolean isSuccess() {
        return result == 1;
    }
}
