package com.huxiu.yd.net.responses;

import com.huxiu.yd.net.model.Comment;
import com.huxiu.yd.net.model.SpareItem;

/**
 * Created by kHRYSTAL on 15/9/15:下午3:26.
 */
public class SpareItemResponse extends BaseResponse {

    public SpareItem data;

    public Comment[] comment;

    public Comment[] featured_comment;
}
