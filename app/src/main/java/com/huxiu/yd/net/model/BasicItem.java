package com.huxiu.yd.net.model;

import com.huxiu.yd.R;
import com.huxiu.yd.utils.LogUtils;

/**
 * Created by kHRYSTAL on 15/9/17.
 */
public class BasicItem {
    public int user_type;

    public int color_type;

    public String color;

    public int getUserDrawable() {
        switch (user_type) {
            case 0:
                return 0;
            case 1:
                return R.drawable.user_type_user;
            case 2:
                return R.drawable.user_type_provider;
        }
        return 0;
    }

    public int getDividerColor() {
        try {
            return 0xff000000 + Integer.parseInt(color, 16);
        } catch (Exception e) {
            return 0xffcccccc;
        }
    }

    public int getProductPlaceHolder() {
        LogUtils.d("getProductPlaceHolder, color type is " + color_type);
        switch (color_type) {
            case 1:
                return R.drawable.placeholder_image_blue;
            case 2:
                return R.drawable.placeholder_image_red;
            case 3:
                return R.drawable.placeholder_image_green;
            case 0:
                return R.drawable.placeholder_image_yellow;
        }
        return R.drawable.default_product_image;
    }
}
