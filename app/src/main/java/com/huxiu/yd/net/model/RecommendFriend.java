package com.huxiu.yd.net.model;

/**
 * Created by kHRYSTAL on 15/9/17.
 */
public class RecommendFriend {
    public String rf_url;
    public String rf_title;
    public String rf_img;
    public String rf_shareId;
    public String rf_share_type;
}
