package com.huxiu.yd.net.model;

/**
 * Created by kHRYSTAL on 15/9/17.
 */
public class BindInfo {
    public int bid;
    public String type;
    public String nick_name;
    public String token;
    public String openid;
}
