package com.huxiu.yd.net.model;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:16.
 */
public class Topic {

    public String title;

    public String id;

    public String image;

    public String description;

    public int member_num;

    public int member_max;

    public long enroll_time;

    public long enroll_end_time;

    public long create_time;

    public int is_return;

    public int status;

    public String content;

    public String explain;

    public String rule;

    public EnrollData[] enroll_data;
}
