package com.huxiu.yd.net.model;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:13.
 */
public class Comment extends BasicItem implements Serializable{

    public String user_id;

    public String id;

    public String name;

    public String avatar;

    public int recommend_num;

    public long create_time;

    public String content;

    public String article_id;

    public String user_goods_id;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
