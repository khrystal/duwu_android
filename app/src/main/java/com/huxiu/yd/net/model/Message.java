package com.huxiu.yd.net.model;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:25.
 */
public class Message {

    public String user_id;

    public String did;

    public int type;

    public String avatar;

    public String content;

    public String name;

    public int status;

    public long create_time;
}
