package com.huxiu.yd.net.responses;

import com.huxiu.yd.net.model.User;

/**
 * Created by kHRYSTAL on 15/9/15.
 */
public class UserListResponse extends BaseResponse {
    public User[] data;
}
