package com.huxiu.yd.net.responses;

import com.huxiu.yd.net.model.ArticleItem;

/**
 * Created by kHRYSTAL on 15/9/16.
 */
public class ArticlesResponse extends BaseResponse {
    public ArticleItem[] data;
}
