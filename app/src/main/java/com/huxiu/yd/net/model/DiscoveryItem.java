package com.huxiu.yd.net.model;

import com.google.gson.Gson;

import com.huxiu.yd.R;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:11.
 */
public class DiscoveryItem extends BasicItem {
    public int user_status;

    public String share_url;

    public int id;

    public int article_id;

    public String image;

    public String title;

    public String name;

    public String user_id;

    public String avatar;

    public int comment_num;

    public String description;

    public int like_num;

    public int like_status;

    public int member_num;

    public int member_max;

    public long enroll_time;

    public long enroll_end_time;

    public long create_time;

    public int is_return;

    public int status;

    public String content;

    public String[] explain;

    public String[] rule;

    public EnrollData[] enroll_data;

    public String company_name;

    public String company_id;

    public int apply_status;

    public boolean liked() {
        return like_status == 1;
    }

    public static final int ENROLL_STATUS_IN_PROGRESS = 1;

    public static final int ENROLL_STATUS_ENDS = 2;

    public static final int ENROLL_STATUS_NOT_BEGIN = 0;

    public int getEnrollStatusPrompt() {
        switch (status) {
            case ENROLL_STATUS_NOT_BEGIN:
                return R.string.coming_soon;
            case ENROLL_STATUS_ENDS:
                return R.string.enroll_ends;
            case ENROLL_STATUS_IN_PROGRESS:
                return R.string.enroll_in_progress;
        }
        return R.string.empty_default_value;
    }

    public int getEnrollStatusPrompt2() {

        switch (status) {
            case ENROLL_STATUS_NOT_BEGIN:
                return R.string.coming_soon;
            case ENROLL_STATUS_ENDS:
                return R.string.enroll_ends;
            case ENROLL_STATUS_IN_PROGRESS:
                if (user_status == 0) {
                    return R.string.already_registered;
                }
                return R.string.register_now;
        }
        return R.string.empty_default_value;
    }

    public int getEnrollStatusColor() {
        switch (status) {
            case ENROLL_STATUS_NOT_BEGIN:
                return R.color.enroll_ends_red;
            case ENROLL_STATUS_ENDS:
                return R.color.gray4;
            case ENROLL_STATUS_IN_PROGRESS:
                return R.color.enroll_in_progress_blue;
        }
        return R.color.white;
    }

    public int getEnrollStatusColor2() {
        if (user_status != 3) {
            return R.color.gray4;
        }
        switch (status) {
            case ENROLL_STATUS_NOT_BEGIN:
                return R.color.enroll_ends_red;
            case ENROLL_STATUS_ENDS:
                return R.color.gray4;
            case ENROLL_STATUS_IN_PROGRESS:
                return R.color.white;
        }
        return R.color.white;
    }

    public int getEnrollButtonBg() {
        if (user_status != 3) {
            return R.drawable.cannot_register_bg;
        }
        switch (status) {
            case ENROLL_STATUS_NOT_BEGIN:
                return R.drawable.about_begin_bg;
            case ENROLL_STATUS_ENDS:
                return R.drawable.cannot_register_bg;
            case ENROLL_STATUS_IN_PROGRESS:
                return R.drawable.login_button;
        }
        return R.drawable.register_button_bg;
    }

    public int getApplyStatusColor() {
        switch (apply_status) {
            case 0:
                return R.color.enroll_in_progress_blue;
            case 1:
                return R.color.enroll_passed;
            case 2:
                return R.color.enroll_ends_red;
        }
        return R.color.enroll_in_progress_blue;
    }

    public int getApplyStatusString() {
        switch (apply_status) {
            case 0:
                return R.string.apply_in_progress;
            case 1:
                return R.string.apply_passed;
            case 2:
                return R.string.apply_failed;
        }
        return R.string.apply_in_progress;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static final int USER_STATUS_NOT_ATTENDED = 3;

    public String getUserStatusText() {
        switch (user_status) {
            case 0:
                return "已报名";
            case 1:
                return "已通过";
            case 2:
                return "未通过";
            default:
                return "立即报名";
        }
    }
}
