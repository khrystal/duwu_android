package com.huxiu.yd.net.responses;

import com.huxiu.yd.net.model.DiscoveryItem;

/**
 * Created by kHRYSTAL on 15/9/15:下午3:26.
 */
public class DiscoveryListResponse extends BaseResponse {

    public DiscoveryItem[] data;
}
