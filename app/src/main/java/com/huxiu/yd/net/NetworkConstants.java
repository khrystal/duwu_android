package com.huxiu.yd.net;

/**
 * Created by kHRYSTAL on 15/9/15:上午7:49.
 */
public class NetworkConstants {

    public static final String BASE_URL = "http://api.yd.dzter.com/";

    public static final String DISCOVERY_URL = BASE_URL + "discover";

    public static final String MEMBER_URL = BASE_URL + "member";

    public static final String THIRD_PARTY_URL = BASE_URL + "login_tp";

    public static final String GOODS_URL = BASE_URL + "goods";

    public static final String TOPIC_URL = BASE_URL + "topic";

}
