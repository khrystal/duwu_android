package com.huxiu.yd.net;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.huxiu.yd.App;
import com.huxiu.yd.utils.LogUtils;


/**
 * Created by kHRYSTAL on 15/9/15:下午1:08.
 */
public class NetworkHelper {

    private static NetworkHelper mInstance;

    private RequestQueue mRequestQueue;


    private NetworkHelper() {
        VolleyLog.DEBUG = LogUtils.DEBUGGABLE;
        mRequestQueue = getRequestQueue();
    }

    public static NetworkHelper getInstance() {
        return mInstance;
    }

    public static void init() {
        mInstance = new NetworkHelper();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(App.getInstance());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }


}
