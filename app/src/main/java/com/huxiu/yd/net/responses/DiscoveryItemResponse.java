package com.huxiu.yd.net.responses;

import com.huxiu.yd.net.model.Comment;
import com.huxiu.yd.net.model.DiscoveryItem;
import com.huxiu.yd.net.model.EnrollData;

/**
 * Created by kHRYSTAL on 15/9/15:下午3:26.
 */
public class DiscoveryItemResponse extends BaseResponse {

    public DiscoveryItem data;

    public Comment[] comment;

    public Comment[] featured_comment;

}
