package com.huxiu.yd.net.model;

import com.google.gson.Gson;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:07.
 */
public class User {

    public String user_id;

    public String yd_key;

    public String yd_auth;

    public String name;

    public String realname;

    public String avatar;

    public String mobile;

    public int is_admin;

    public long create_time;

    public String yijuhua;

    public String audit;

    public String nickname;

    public String email;

    public String contact;

    public String openid;
    public String token;

    public BindInfo[] bind;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
