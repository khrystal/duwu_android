package com.huxiu.yd.net.model;

import com.google.gson.Gson;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:20.
 */
public class SpareItem extends BasicItem{

    public String name;

    public String user_goods_id;

    public long create_time;

    public String[] images;

    public String title;

    public String description;

    public String user_id;

    public String avatar;

    public int like_status;

    public int like_num;

    public String city_name;

    public String price;

    public int comment_num;

    public String content;

    public Comment[] comment;

    public ArticleItem[] related_articles;

    public boolean liked() {
        return like_status == 1;
    }

    public String share_url;

    public String mobile;

    public String contact_name;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
