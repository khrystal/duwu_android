package com.huxiu.yd.net;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.huxiu.yd.R;
import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Global;
import com.huxiu.yd.utils.ImageUtils;
import com.huxiu.yd.utils.LogUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by kHRYSTAL on 15/9/15:下午2:27.
 */
public class UploadImageRequest extends AsyncTask<Void, Void, String> {

    private Context context;

    private String url;

    private String key;

    private Bitmap bitmap;

    public interface ResultListener {

        public void onSuccess(String msg);

        public void onFailure(String msg);
    }

    private ResultListener listener;

    @Override
    protected String doInBackground(Void... params) {
        byte[] data = ImageUtils.getBytesFromBitmap(bitmap);
        ByteArrayBody body = new ByteArrayBody(data, ContentType.MULTIPART_FORM_DATA, "avatar.jpg");
        MultipartEntityBuilder entity = MultipartEntityBuilder.create();
        entity.addTextBody("Appid", Constants.APP_ID);
        entity.addTextBody("Appkey", Constants.APP_SECRET);
        entity.addTextBody("mid", Global.DeviceUniqueID);
        entity.addTextBody("platform", "android");
        entity.addTextBody("client_ver", Global.version);
        entity.addTextBody("yd_auth", Global.user.yd_auth);
        entity.addTextBody("yd_key", Global.user.yd_key);
        entity.addTextBody("yd_uid", Global.user.user_id);
        entity.addTextBody("act", "up_avatar");
        LogUtils.d("doInBackground, url is " + url);
        entity.addPart("avatar", body);
        HttpPost post = new HttpPost(url);
        post.setEntity(entity.build());
        HttpClient client = new DefaultHttpClient();
        try {
            HttpResponse resp = client.execute(post);
            if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return EntityUtils.toString(resp.getEntity());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public UploadImageRequest(Context context, String url, String key, Bitmap bitmap,
                              ResultListener listener) {
        super();
        this.context = context;
        this.url = url;
        this.key = key;
        this.bitmap = bitmap;
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        LogUtils.d("upload image, result is " + result);
        try {
            JSONObject object = new JSONObject(result);
            if (object.optInt("result") == 1) {
                listener.onSuccess(result);
            } else {
                listener.onFailure(object.optString("msg"));
            }
        } catch (Exception e) {
            listener.onFailure(context.getString(R.string.upload_picture_failed));
        }
    }
}
