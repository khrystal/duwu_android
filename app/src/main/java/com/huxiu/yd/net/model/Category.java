package com.huxiu.yd.net.model;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:23.
 */
public class Category implements Serializable{

    public String id;

    public String name;

    public String title;

    public String product_id;
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
