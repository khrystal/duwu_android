package com.huxiu.yd.net.model;

import com.google.gson.Gson;

import com.huxiu.yd.R;

/**
 * Created by kHRYSTAL on 15/9/17:下午6:26.
 */
public class Action {

    public long favorite_time;

    public String title;

    public String id;

    public String type;

    public int getName() {
        if (type.equals("user_goods")) {
            return R.string.spare;
        } else {
            return R.string.massive_test;
        }
    }

    public boolean isMassiveTest() {
        return !type.equals("user_goods");
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
