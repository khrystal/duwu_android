package com.huxiu.yd;

import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

public class MiUserReceiver extends BroadcastReceiver {
    public MiUserReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return;
        }
        switch (action) {
            case Constants.INTENT_USER_LOGGED_IN:
                MiPushReceiver.registerPush(Settings.getMiId(), "login");
                break;
            case Constants.INTENT_USER_LOGGED_OUT:
                MiPushReceiver.registerPush(Settings.getMiId(), "logout");
                break;
        }
    }
}
