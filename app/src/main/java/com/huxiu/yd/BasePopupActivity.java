package com.huxiu.yd;

import com.huxiu.yd.utils.LogUtils;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created by yao on 15/7/25:下午9:06.
 */
public class BasePopupActivity extends BaseActivity {

    protected View background;

    protected View content;

    @Override
    public void finish() {
        background.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        super.finish();
        overridePendingTransition(R.anim.push_out_to_bottom, R.anim.push_out_to_bottom);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.push_in_from_bottom);
        LogUtils.d("BasePopupActivity, onStart, content is " + content);
        content.startAnimation(animation);
        AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setDuration(200);
        animation1.setFillAfter(true);
        background.startAnimation(animation1);
    }

}
