package com.huxiu.yd;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import com.huxiu.yd.fragments.DiscoveryFragment;
import com.huxiu.yd.fragments.MassiveTestFragment;
import com.huxiu.yd.fragments.ProfileFragment;
import com.huxiu.yd.fragments.SpareFragment;
import com.umeng.update.UmengUpdateAgent;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends BaseActivity implements OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private String[] tabs = new String[4];

    private int currentFragmentIndex;

    private Fragment currentFragment;

    private Map<Integer, Fragment> fragments = new HashMap<>(4);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        UmengUpdateAgent.update(this);
        tabs[0] = getString(R.string.discover);
        tabs[1] = getString(R.string.massive_test);
        tabs[2] = getString(R.string.spare);
        tabs[3] = getString(R.string.me);
        findViewById(R.id.discovery_button).setOnClickListener(this);
        findViewById(R.id.massive_test_button).setOnClickListener(this);
        findViewById(R.id.spare_button).setOnClickListener(this);
        findViewById(R.id.me_button).setOnClickListener(this);
        setupFragments(savedInstanceState);
    }

    private void setupFragments(Bundle savedInstanceState) {
        if (fragments.size() != 4) {
            fragments.put(R.id.discovery_button,
                    Fragment.instantiate(this, DiscoveryFragment.class.getName()));
            fragments.put(R.id.massive_test_button,
                    Fragment.instantiate(this, MassiveTestFragment.class.getName()));
            fragments.put(R.id.spare_button,
                    Fragment.instantiate(this, SpareFragment.class.getName()));
            fragments.put(R.id.me_button,
                    Fragment.instantiate(this, ProfileFragment.class.getName()));
            currentFragment = fragments.get(R.id.discovery_button);
            currentFragmentIndex = R.id.discovery_button;
        }

        if (savedInstanceState == null) {
            currentFragmentIndex = R.id.discovery_button;
        } else {
            currentFragmentIndex = savedInstanceState.getInt("tab");
            while (getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStack();
            }
        }
        Fragment fragment = fragments.get(currentFragmentIndex);
        getFragmentManager().beginTransaction().add(R.id.fragments, fragment).show(fragment)
                .commitAllowingStateLoss();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.discovery_button:
                switchFragment(v);
                break;
            case R.id.massive_test_button:
                switchFragment(v);
                break;
            case R.id.spare_button:
                switchFragment(v);
                break;
            case R.id.me_button:
                switchFragment(v);
                break;
            default:
                break;
        }
    }

    private void switchFragment(View v) {
        if (currentFragmentIndex == v.getId()) {
            return;
        }
        int id = v.getId();
        Fragment fragment1 = fragments.get(id);
        Fragment fragment2 = fragments.get(currentFragmentIndex);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (!fragment1.isAdded()) {
            ft.add(R.id.fragments, fragment1);
        }
        ft.hide(fragment2).show(fragment1).commitAllowingStateLoss();

        currentFragmentIndex = id;
        RadioButton button = (RadioButton) findViewById(id);
        button.setChecked(true);

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("tab", currentFragmentIndex);
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        for (int i = 0; i < 4; i++) {
            Fragment fragment = fm.findFragmentByTag(String.valueOf(i));
            if (i != currentFragmentIndex && fragment != null) {
                ft.hide(fragment);
            }
        }
        ft.commitAllowingStateLoss();
    }
}
