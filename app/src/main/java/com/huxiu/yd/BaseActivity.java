package com.huxiu.yd;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.webkit.WebView;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.huxiu.yd.fragments.ProgressDialogFragment;
import com.huxiu.yd.utils.ErrorResponseException;
import com.huxiu.yd.utils.Utils;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by tian on 14-9-20:下午10:16.
 */
public class BaseActivity extends Activity {

    protected ProgressDialogFragment progressDialogFragment;

    protected void showProgress() {
        showProgress(null, null);
    }

    protected void showProgress(int progressMessageId, int progressTitleId) {
        showProgress(getString(progressMessageId), getString(progressTitleId));
    }

    protected void showProgress(String progressMessage, String progressTitle) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        progressDialogFragment = ProgressDialogFragment.newInstance(progressTitle, progressMessage);
        try {
            progressDialogFragment.show(ft, "dialog");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void showProgress(String progressMessage) {
        showProgress(progressMessage, null);
    }

    protected void showProgress(int progressMessageId) {
        showProgress(getString(progressMessageId));
    }

    protected void dismissProgress() {
        if (progressDialogFragment != null) {
            progressDialogFragment.dismissAllowingStateLoss();
            progressDialogFragment = null;
        }
    }

    protected RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQueue = Volley.newRequestQueue(this);
    }

    @Override
    protected void onDestroy() {
        mQueue.cancelAll(this);
        super.onDestroy();
    }

    protected ErrorListener mErrorListener = new ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            dismissProgress();
            Throwable t = error.getCause();
            if (t instanceof ErrorResponseException) {
                Utils.showToast(error.getMessage());
            } else {
                Utils.showToast(R.string.generic_failure);
            }
        }
    };

    public void updateWebViewSettings(WebView web) {
//        WebSettings webSettings =   web.getSettings();
////        webSettings.setUseWideViewPort(false);
////        webSettings.setLoadWithOverviewMode(false);
//        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        Utils.setDefaultWebSettings(web);
    }

    public void webviewSetContent(WebView web, String content) {
        web.loadData(content, "text/html; charset=UTF-8", null);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
