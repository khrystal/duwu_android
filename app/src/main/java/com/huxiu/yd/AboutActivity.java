package com.huxiu.yd;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.huxiu.yd.utils.Constants;
import com.huxiu.yd.utils.Utils;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by yao on 15/8/26.
 */
public class AboutActivity extends BaseActivity implements View.OnClickListener {

    @InjectView(R.id.title)
    TextView title;
    @InjectView(R.id.back)
    ImageView back;
    @InjectView(R.id.left_text)
    TextView leftText;
    @InjectView(R.id.right_image)
    ImageView rightImage;
    @InjectView(R.id.right_text)
    TextView rightText;
    @InjectView(R.id.user_agreement)
    TextView userAgreement;
    @InjectView(R.id.version)
    TextView version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        ButterKnife.inject(this);
        back.setOnClickListener(this);
        title.setText(R.string.about_us);
        userAgreement.setOnClickListener(this);
        version.setText("V" + Utils.getVersionName(this));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.user_agreement: {
                Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra(Constants.WEB_TITLE_KEY, getString(R.string.user_agreement));
                intent.putExtra(Constants.URL_KEY, "file:///android_asset/user_agreement.html");
                startActivity(intent);
            }
            break;
        }
    }
}
